#!/bin/bash
set -euo pipefail

function usage {
    echo "usage: download.sh <parameter_profile_file> <target_dir>"
    exit 1
}

if [ $# -ne 2 ]; then
    usage
fi
PARAMETER_PROFILE_FILE=$1
TARGET_DIR=$2

if [ ! -f $PARAMETER_PROFILE_FILE ]; then
    echo "Parameter profile file not found: $PARAMETER_PROFILE_FILE"
    usage
fi


if [ ! -d $TARGET_DIR ]; then
    echo "Target dir not found: $TARGET_DIR"
    usage
fi


echo "download data to target dir: $TARGET_DIR"

# Your code here
PPM_URL=`jq -r '.ppm_url' $PARAMETER_PROFILE_FILE`
CODE_DEP=`jq -r '.code_dep' $PARAMETER_PROFILE_FILE`
CONTOURS_URL=`jq -r '.contours_url' $PARAMETER_PROFILE_FILE`

# Although it is not ideal (we depend on correct parameter profile files!) the file names are pre-defined 
PPM_FILE=$CODE_DEP"ppm.csv.gz"
CONTOURS_FILE="cadastre-"$CODE_DEP"-parcelles.json.gz"
echo "$PPM_URL"
wget -q "$PPM_URL" -O "$TARGET_DIR/$PPM_FILE"
echo "$CONTOURS_URL$CODE_DEP/$CONTOURS_FILE"
wget -q "$CONTOURS_URL$CODE_DEP/$CONTOURS_FILE" -O "$TARGET_DIR/$CONTOURS_FILE"

echo "done."
