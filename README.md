# OpenMobility indicator Jupyter notebook "parcelles publiques"

## description : [indicator.yml file](https://gitlab.com/open-mobility-indicators/indicators/parcelles-publiques/-/blob/main/indicator.yaml)

This python notebook produces a GeoJSON file containing the land parcels where the owner is a legal person (such as a company or a public administration) from data published by the French ministry of budget.
(the OMI framework then produces vector tiles as .mbtiles files from the geoJSON file).

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute)](https://gitlab.com/open-mobility-indicators/website/-/wikis/2_contributeur_technique/install-a-notebook-locally)  